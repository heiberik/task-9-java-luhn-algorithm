package com.company.utility;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgoTest {


    @Test
    void testExceptionThrown(){
        assertThrows(Exception.class, () -> {
           LuhnAlgo.validateNumbers("");
        });
    }

    @Test
    void testValidNumbers() throws Exception {
        // I know it is bad practise to throw exceptions like this D:

        String testSequenceValid = "4242424242424242";
        String[] solutionValid = LuhnAlgo.validateNumbers(testSequenceValid);
        assertEquals("424242424242424 2", solutionValid[0]);
        assertEquals("2", solutionValid[1]);
        assertEquals("2", solutionValid[2]);
        assertEquals("Valid", solutionValid[3]);
    }

    @Test
    void testInvalidNumbers() throws Exception  {

        String testSequenceInvalid = "4242424242424245";
        String[] solutionInvalid = LuhnAlgo.validateNumbers(testSequenceInvalid);

        assertEquals("424242424242424 5", solutionInvalid[0]);
        assertEquals("5", solutionInvalid[1]);
        assertEquals("2", solutionInvalid[2]);
        assertEquals("Invalid", solutionInvalid[3]);
    }


    @Test
    void testSumNumbers() {
        int[] digits = {1,2,3,4,5};
        int sum = LuhnAlgo.sumNumbers(digits);
        assertEquals(15, sum);
    }

    
    @Test
    void testIfCreditCard() throws Exception {

        String testSequenceValid = "4242424242424242";
        String testSequenceInvalid = "424242424";

        String[] solutionValid = LuhnAlgo.validateNumbers(testSequenceValid);
        String[] solutionInvalid = LuhnAlgo.validateNumbers(testSequenceInvalid);

        assertEquals("16 (credit card)", solutionValid[4]);
        assertEquals(testSequenceInvalid.length() + " (not a credit card)", solutionInvalid[4]);
    }
}