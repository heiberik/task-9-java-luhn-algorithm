package com.company;

import com.company.utility.LuhnAlgo;
import com.company.utility.TerminalHandler;

public class Program {

    public static void main(String[] args) {

        validateNumbers();
    }

    public static void validateNumbers(){


        // Get user input
        TerminalHandler input = new TerminalHandler();
        String numbers = input.getUserInput();

        if (!numbers.equals("")) {

            try {
                // Validate user input
                String[] solution = LuhnAlgo.validateNumbers(numbers);
                input.printSolution(solution);

                // call func recursive.
                validateNumbers();
            }
            catch(Exception e){
                System.out.println(e);
            }

        }
    }
}
