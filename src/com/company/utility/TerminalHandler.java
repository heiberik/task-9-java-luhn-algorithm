package com.company.utility;

import java.util.Scanner;

public class TerminalHandler {

    public String getUserInput(){
        Scanner scan = null;
        try {
            scan = new Scanner(System.in);

            System.out.println("Press enter to exit.");
            System.out.print("Enter number sequence: ");
            String input = scan.nextLine().trim();
            System.out.println(input);
            if (input.equals("")) return input;

            // check if input can be parsed to a number-sequence.
            String[] numbers = input.split("");
            for (String number : numbers){
                Integer.parseInt(number);
            }
            return input;
        }
        catch (Exception e){
            System.out.println("Input must be a number sequence!");
            return getUserInput();
        }
    }

    public void printSolution(String[] solution){

        System.out.println("\n\tInput:\t\t" + solution[0]);
        System.out.println("\tProvided:\t" + solution[1]);
        System.out.println("\tExpected:\t" + solution[2]);
        System.out.println("\n\tChecksum:\t" + solution[3]);
        System.out.println("\tDigits:\t\t" + solution[4] + "\n");
    }
}
