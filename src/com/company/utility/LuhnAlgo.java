package com.company.utility;

import java.util.Arrays;

public class LuhnAlgo {

    public static String[] validateNumbers(String numbers) throws Exception {

        // Only for testing purposes (So i can test assertThrows.
        // TerminalHandler makes sure an empty String cannot be provided.
        if (numbers.equals("")) throw new Exception();

        // Array which contains information about the validation
        // 0 - Input
        // 1 - Provided
        // 2 - Expected
        // 3 - Checksum
        // 4 - Digits
        String[] solution = new String[5];
        solution[1] = numbers.charAt(numbers.length() - 1) + "";
        solution[0] = numbers.substring(0, numbers.length() - 1) + " " + solution[1];


        // Convert String to integer array of all the numbers in the sequence.
        int[] integerArray = new int[numbers.length()];
        for(int i = 0; i < numbers.length(); i++){
            char c = numbers.charAt(i);
            integerArray[i] = Integer.parseInt(""+c);
        }


        // perform algo.
        for(int i = integerArray.length - 2; i >= 0; i = i-2){
            int num = integerArray[i];
            num = num * 2;
            if(num > 9){
                num = num % 10 + num / 10;
            }
            integerArray[i] = num;
        }

        // get sum of all numbers in sequence.
        int sum = sumNumbers(integerArray);

        // check if checksum is correct and card is valid
        if(sum % 10 == 0){
            solution[2] = solution[1];
            solution[3] = "Valid";
        }
        // if checksum is not correct, find correct checksum.
        else {
            int excess = sum % 10;
            int last = Integer.parseInt(solution[1]);

            int checksum = last - excess;
            if (checksum < 0){checksum = 10 + checksum;}

            solution[2] = checksum + "";
            solution[3] = "Invalid";
        }

        // Check if credit card
        if (numbers.length() == 16){
            solution[4] = "16 (credit card)";
        }
        else {
            solution[4] = numbers.length() + " (not a credit card)";
        }

        // return the solution array.
        return solution;
    }

    public static int sumNumbers(int[] array){
        return Arrays.stream(array).sum();
    }
}
